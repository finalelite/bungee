package com.bungeefinal.apis;

import com.bungeefinal.MySql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserIP {

    public static String getIP(String nick) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM User_ip WHERE Nick = ?");
            st.setString(1, nick);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getString("Ip");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
