package com.bungeefinal.apis;

import com.bungeefinal.MySql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class TagAPI {

    public static String getTag(String uuid) {
        if (get(uuid) != null) return get(uuid).getTag();
        return null;
    }

    public static String getTagDb(String UUID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Tag_data WHERE UUID = ?");
            st.setString(1, UUID);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getString("Tag");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    //-

    public static void updateTag(String uuid) {
        cache.remove(uuid);
        new TagAPI(uuid, getTagDb(uuid)).insert();
    }

    public static void setInfo(String uuid) {
        new TagAPI(uuid, getTagDb(uuid)).insert();
    }

    private String uuid;
    private String tag;

    public static HashMap<String, TagAPI> cache = new HashMap<>();

    public TagAPI (String uuid, String tag) {
        this.uuid = uuid;
        this.tag = tag;
    }

    public static TagAPI get(String uuid) {
        return cache.get(uuid);
    }

    public TagAPI insert() {
        cache.put(this.uuid, this);
        return this;
    }

    public String getTag() {
        return this.tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
