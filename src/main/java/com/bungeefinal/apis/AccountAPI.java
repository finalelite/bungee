package com.bungeefinal.apis;

import com.bungeefinal.MySql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AccountAPI {

    public static void setAccountInfo(String UUID, String nick) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Account_data(UUID, Nick) VALUES (?, ?)");
            st.setString(1, UUID);
            st.setString(2, nick);
            //st.setBoolean(3, premium);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateNick(String UUID, String nick) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("UPDATE Account_data SET Nick = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setString(1, nick);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static String getNick(String UUID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Account_data WHERE UUID = ?");
            st.setString(1, UUID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Nick");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getUUID(String nick) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Account_data WHERE Nick = ?");
            st.setString(1, nick);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getString("UUID");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
