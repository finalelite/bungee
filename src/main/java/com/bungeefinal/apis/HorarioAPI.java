package com.bungeefinal.apis;

public class HorarioAPI {

    public static String getTime(long secs) {

        long days = secs / 86400;
        long hours = (secs % 86400) / 3600;
        long minutes = (secs % 3600) / 60;
        long seconds = secs % 60;

        StringBuilder sb = new StringBuilder();
        if (days >= 1) {
            sb.append(days);
            sb.append(days == 1 ? " dia " : " dias ");
        }
        if (hours >= 1) {
            sb.append(hours);
            sb.append(hours == 1 ? " hora " : " horas ");
        }
        if (minutes >= 1) {
            sb.append(minutes);
            sb.append(minutes == 1 ? " minuto " : " minutos ");
        }
        if (seconds >= 1) {
            sb.append("e ");
            sb.append(seconds);
            sb.append(seconds == 1 ? " segundo " : " segundos ");
        }
        return sb.toString().trim();
    }
}
