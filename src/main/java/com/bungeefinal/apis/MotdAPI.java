package com.bungeefinal.apis;

import com.bungeefinal.MySql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class MotdAPI {

    public static String getPingDb() {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM motd");
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getString("Ping");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getBackPingDb() {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM motd");
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getString("Back_ping");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getFirstLineDb() {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM motd");
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getString("First_line");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getSecondLineDb() {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM motd");
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getString("Second_line");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    //---

    public static void updateMotd() {
        get().setPing(getPingDb());
        get().setBackPing(getBackPingDb());
        get().setFirstLine(getFirstLineDb());
        get().setSecondLine(getSecondLineDb());
    }

    public static void enableMotd() {
        new MotdAPI("motd", getPingDb(), getBackPingDb(), getFirstLineDb(), getSecondLineDb()).insert();
    }

    private String motd;
    private String ping;
    private String backPing;
    private String firstLine;
    private String secondLine;

    public static HashMap<String, MotdAPI> cache = new HashMap<>();

    public MotdAPI (String motd, String ping, String backPing, String firstLine, String secondLine) {
        this.motd = motd;
        this.ping = ping;
        this.backPing = backPing;
        this.firstLine = firstLine;
        this.secondLine = secondLine;
    }

    public static MotdAPI get() {
        return cache.get("motd");
    }

    public MotdAPI insert() {
        cache.put(this.motd, this);
        return this;
    }

    public void setSecondLine(String secondLine) {
        this.secondLine = secondLine;
    }

    public String getSecondLine() {
        return this.secondLine;
    }

    public void setFirstLine(String firstLine) {
        this.firstLine = firstLine;
    }

    public String getFirstLine() {
        return this.firstLine;
    }

    public void setBackPing(String backPing) {
        this.backPing = backPing;
    }

    public String getBackPing() {
        return this.backPing;
    }

    public void setPing(String ping) {
        this.ping = ping;
    }

    public String getPing() {
        return this.ping;
    }
}
