package com.bungeefinal.listeners;

import com.bungeefinal.apis.IsLoggedAPI;
import com.bungeefinal.apis.TagAPI;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class PlayerDisconnectEvents implements Listener {

    @EventHandler
    public void onLeft(PlayerDisconnectEvent event) {
        ProxiedPlayer player = event.getPlayer();
        String uuid = player.getUniqueId().toString();

        if (IsLoggedAPI.getLoggedStatusExist(player.getUniqueId().toString()) != null) {
            IsLoggedAPI.updateLoggedStatus(player.getUniqueId().toString(), false);
        }

        TagAPI.cache.remove(uuid);
    }
}
