package com.bungeefinal.listeners;

import com.bungeefinal.Main;
import com.bungeefinal.MySql;
import com.bungeefinal.apis.HorarioAPI;
import com.bungeefinal.apis.PunirAPI;
import com.bungeefinal.apis.TagAPI;
import com.bungeefinal.apis.UserIP;
import com.bungeefinal.apis.AccountAPI;
import com.bungeefinal.utils.CentralizeMsg;
import com.mrpowergamerbr.temmiewebhook.DiscordMessage;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.event.PreLoginEvent;
import net.md_5.bungee.api.event.ServerConnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Date;

public class PlayerConnectEvents implements Listener {

    @EventHandler
    public void onJoin(ServerConnectEvent event) {
        ProxiedPlayer player = event.getPlayer();
        String uuid = player.getUniqueId().toString();

        if (!AccountAPI.getNick(uuid).equalsIgnoreCase(player.getName())) {
            player.disconnect(new TextComponent("Algo deu errado (uuid Spoof?)"));
        }
    }

    @EventHandler
    public void onLogin(PostLoginEvent event) {
        if (AccountAPI.getNick(event.getPlayer().getUniqueId().toString()) == null) {
            AccountAPI.setAccountInfo(event.getPlayer().getUniqueId().toString(), event.getPlayer().getName());
        }
    }

    @EventHandler
    public void loginEvent(LoginEvent event) {
        String nick = event.getConnection().getName();
        String uuid = event.getConnection().getUniqueId().toString();
        
        String databaseUUID = AccountAPI.getUUID(nick);
        String databaseName = AccountAPI.getNick(databaseUUID);


        if (databaseName != null && !databaseName.equals(nick)) {
            event.getConnection().disconnect(new TextComponent(
                    ChatColor.RED + "Esse nome já foi usado antes como " + databaseName + ". Por favor, conecte-se usando ele."));
        }

        if (nick != null && PunirAPI.getExistPunicao(uuid) != null) {
            if (!PunirAPI.getIsMutado(uuid)) {
                if (PunirAPI.getIsEterno(uuid)) {
                    String msg = CentralizeMsg.sendCenteredMessage(ChatColor.RED + "§lFinal Elite\n \n§cVocê foi banido permanentemente do servidor.\n \nMotivo: " + ChatColor.GRAY + PunirAPI.getMotivo(uuid) + ChatColor.RED + "\nAutor: " + ChatColor.GRAY + PunirAPI.getAutor(uuid) + ChatColor.RED +
                            "\nProva: " + ChatColor.GRAY + PunirAPI.getProva(uuid) + ChatColor.RED + "\nID do caso: " + ChatColor.DARK_AQUA + PunirAPI.getCaseId(uuid) + ChatColor.RED + "\n \nFoi punido incorretamente? Mande-nos uma DM no Twitter " + ChatColor.BLUE + "@FinalElite");
                    event.getConnection().disconnect(new TextComponent(msg));
                } else {
                    if ((PunirAPI.getTempo(uuid).getTime() / 1000 ) - (new Date().getTime() / 1000) >= 1) {
                        String msg = CentralizeMsg.sendCenteredMessage(ChatColor.RED + "§lFinal Elite\n \n§cVocê foi banido temporariamente do servidor.\n \nMotivo: " + ChatColor.GRAY + PunirAPI.getMotivo(uuid) + ChatColor.RED + "\nAutor: " + ChatColor.GRAY + PunirAPI.getAutor(uuid) +
                                ChatColor.RED + "\nProva: " + ChatColor.GRAY + ChatColor.RED + "\nID do caso: " + ChatColor.DARK_AQUA + PunirAPI.getProva(uuid) + ChatColor.RED + "\nDuração: " + ChatColor.GRAY + HorarioAPI.getTime((PunirAPI.getTempo(uuid).getTime() / 1000) - (new Date().getTime() / 1000))
                                + ChatColor.RED + ChatColor.RED + "\n \nFoi punido incorretamente? Crie um ticket em nosso discord " + ChatColor.BLUE + "discord.me/finalelite");

                        event.getConnection().disconnect(new TextComponent(msg));
                    } else {
                        PunirAPI.removerPunicao(uuid);
                    }
                }
            }
        }

        TagAPI.setInfo(uuid);

        String[] staff = {"Master", "Supervisor", "Admin", "Moderador", "Suporte"};
        String ip = UserIP.getIP(nick);
        if (Arrays.asList(staff).contains(TagAPI.getTag(uuid)) || ip != null) {
            if (ip == null || !ip.equalsIgnoreCase(event.getConnection().getAddress().getHostString())) {
                String msg = CentralizeMsg.sendCenteredMessage(ChatColor.RED + "§lFinal Elite    \n \n" + ChatColor.RED + "Você não pode acessar esta conta!");
                event.getConnection().disconnect(new TextComponent(msg));
                try {
                    PreparedStatement st = MySql.con.prepareStatement("SELECT discordId FROM Discord_info WHERE nick = ?");
                    st.setString(1, nick);
                    ResultSet rs = st.executeQuery();
                    if (rs.next()) {
                        DiscordMessage privateMessage = new DiscordMessage
                                ("Watchdog", String.format(":warning: Acesso não permitido feito à conta de <@%s> (%s) pelo IP %s.",
                                        rs.getString("discordId"),
                                        nick,
                                        event.getConnection().getAddress().getHostString()),
                                        "https://i.imgur.com/bpgg6oh.jpg");
                        Main.privateTeam.sendMessage(privateMessage);
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    @EventHandler
    public void preLogin(PreLoginEvent event) {
        String uuid = event.getConnection().getUniqueId().toString();
        String connectionName = event.getConnection().getName();
        
        if (!connectionName.matches("\\w*")) {
            event.getConnection().disconnect(new TextComponent(
                    ChatColor.RED + "O nome " + connectionName + " não é válido. Use apenas letras, números e '_'."));
            event.setCancelled(true);
        }

        event.getConnection().setOnlineMode(false);

        //if (connectionName != null && PunirAPI.getExistPunicao(uuid) != null) {
        //            if (!PunirAPI.getIsMutado(uuid)) {
        //                if (PunirAPI.getIsEterno(uuid)) {
        //                    String msg = CentralizeMsg
        //                            .sendCenteredMessage(ChatColor.RED + "§lFinal Elite\n \n§cVocê está banido permanentemente do servidor." +
        //                                    "\nMotivo: " + ChatColor.GRAY + PunirAPI.getMotivo(uuid) + ChatColor.RED + "" +
        //                                    "\nAutor: " + ChatColor.GRAY + PunirAPI.getAutor(uuid) + ChatColor.RED + "\n " +
        //                                    "\nFoi punido incorretamente? Mande-nos uma DM no Twitter " + ChatColor.BLUE + "@FinalElite");
        //                    event.getConnection().disconnect(new TextComponent(msg));
        //                } else {
        //                    if (PunirAPI.getTempo(uuid) >= 1) {
        //                        String msg = CentralizeMsg.sendCenteredMessage(ChatColor.RED + "§lFinal Elite\n \n§cVocê está banido temporariamente do servidor." +
        //                                "\nMotivo: " + ChatColor.GRAY + PunirAPI.getMotivo(uuid) + ChatColor.RED + "" +
        //                                "\nDuração: " + ChatColor.GRAY + HorarioAPI.getTime(PunirAPI.getTempo(uuid)) + ChatColor.RED + "" +
        //                                "\nAutor: " + ChatColor.GRAY + PunirAPI.getAutor(uuid) + ChatColor.RED + "\n " +
        //                                "\nFoi punido incorretamente? Mande-nos uma DM no Twitter " + ChatColor.BLUE + "@FinalElite");
        //                        event.getConnection().disconnect(new TextComponent(msg));
        //                    } else {
        //                        PunirAPI.removerPunicao(uuid);
        //                    }
        //                }
        //            }
        //        }
        //
        //        String[] staff = {"Master", "Supervisor", "Admin", "Moderador", "Suporte"};
        //        if (Arrays.asList(staff).contains(TagAPI.getTag(uuid)) || UserIP.getIP(connectionName) != null) {
        //            if (UserIP.getIP(connectionName) == null || !UserIP.getIP(connectionName).equalsIgnoreCase(event.getConnection().getAddress().getHostString())) {
        //                String msg = CentralizeMsg.sendCenteredMessage(ChatColor.RED + "§lFinal Elite    \n \n" + ChatColor.RED + "Você não pode acessar esta conta!");
        //                event.getConnection().disconnect(new TextComponent(msg));
        //                try {
        //                    PreparedStatement st = MySql.con.prepareStatement("SELECT discordId FROM Discord_info WHERE nick = ?");
        //                    st.setString(1, connectionName);
        //                    ResultSet rs = st.executeQuery();
        //                    if (rs.next()) {
        //                        DiscordMessage privateMessage = new DiscordMessage
        //                                ("Watchdog", String.format(":warning: Acesso não permitido feito à conta de <@%s> (%s) pelo IP %s.",
        //                                        rs.getString("discordId"),
        //                                        connectionName,
        //                                        event.getConnection().getAddress().getHostString()),
        //                                        "https://i.imgur.com/bpgg6oh.jpg");
        //                        Main.privateTeam.sendMessage(privateMessage);
        //                    }
        //                } catch (SQLException e) {
        //                    e.printStackTrace();
        //                }
        //            }
        //        }
    }
}
