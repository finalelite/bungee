package com.bungeefinal.utils;

import com.bungeefinal.Main;
import com.google.common.io.ByteStreams;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public class ConfigurationUtils {

    public File load() {
        File folder = Main.getInstance().getDataFolder();

        if(!folder.exists()) {
            folder.mkdir();
        }

        File rFile = new File(folder, "config.yml");

        try {
            if(!rFile.exists()) {
                rFile.createNewFile();
                InputStream in = Main.getInstance().getResourceAsStream("config.yml");
                OutputStream out = new FileOutputStream(rFile);

                ByteStreams.copy(in, out);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return rFile;
    }
}
