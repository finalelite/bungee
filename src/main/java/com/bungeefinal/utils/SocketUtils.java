package com.bungeefinal.utils;

import com.bungeefinal.Main;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

import static com.bungeefinal.apis.ServerAPI.getPort;
import static com.bungeefinal.apis.ServerAPI.updateOnlineStatus;

public class SocketUtils {

    public static void online() {
        try {
            Socket s = new Socket();
            s.connect(new InetSocketAddress(Main.ip, getPort("Hub1")));
            s.close();

            updateOnlineStatus("Hub1", true);
        } catch (IOException e) {
            updateOnlineStatus("Hub1", false);
        }

        try {
            Socket s = new Socket();
            s.connect(new InetSocketAddress(Main.ip, getPort("Hub2")));
            s.close();

            updateOnlineStatus("Hub2", true);
        } catch (IOException e) {
            updateOnlineStatus("Hub2", false);
        }

        try {
            Socket s = new Socket();
            s.connect(new InetSocketAddress(Main.ip, getPort("Hub2")));
            s.close();

            updateOnlineStatus("Hub3", true);
        } catch (IOException e) {
            updateOnlineStatus("Hub3", false);
        }

        try {
            Socket s = new Socket();
            s.connect(new InetSocketAddress(Main.ip, getPort("Factions")));
            s.close();

            updateOnlineStatus("Factions", true);
        } catch (IOException e) {
            updateOnlineStatus("Factions", false);
        }

        try {
            Socket s = new Socket();
            s.connect(new InetSocketAddress(Main.ip, getPort("Construir")));
            s.close();

            updateOnlineStatus("Construir", true);
        } catch (IOException e) {
            updateOnlineStatus("Construir", false);
        }
    }
}
