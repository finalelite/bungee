package com.bungeefinal.commands;

import com.bungeefinal.apis.MotdAPI;
import com.bungeefinal.apis.TagAPI;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class CommandMotd extends Command {

    public CommandMotd() {
        super("motd");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (sender instanceof ProxiedPlayer) {
            ProxiedPlayer player = (ProxiedPlayer) sender;

            String tag = TagAPI.getTag(player.getUniqueId().toString());
            if (tag.equalsIgnoreCase("Master") || tag.equalsIgnoreCase("Supervisor")) {
                player.sendMessage(ChatColor.GREEN + " * Motd atualizado!");
                MotdAPI.updateMotd();
            }
        } else {
            sender.sendMessage(ChatColor.GREEN + " * Motd atualizado!");
            MotdAPI.updateMotd();
        }
    }
}
