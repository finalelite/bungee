package com.bungeefinal.commands;

import com.bungeefinal.apis.TagAPI;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.Arrays;

public class CommandUpdateTag extends Command {

    public CommandUpdateTag() {
        super("tagupdate");
    }

    @Override
    public void execute(CommandSender sender, String[] strings) {
        if (!(sender instanceof ProxiedPlayer)) {
            ProxiedPlayer target = ProxyServer.getInstance().getPlayer(strings[0]);
            if (target != null) {
                TagAPI.updateTag(target.getUniqueId().toString());
            }
        }
    }
}
