package com.bungeefinal.commands;

import com.bungeefinal.apis.TagAPI;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import java.util.Arrays;
import java.util.Collection;

public class CommandStaff extends Command {

    public CommandStaff() {
        super("s");
    }

    @Override
    public void execute(CommandSender sender, String[] strings) {
        if (sender instanceof ProxiedPlayer) {
            ProxiedPlayer player = (ProxiedPlayer) sender;
            String tag = TagAPI.getTag(player.getUniqueId().toString());

            String[] staff = {"Master", "Supervisor", "Admin", "Moderador", "Suporte"};
            if (Arrays.asList(staff).contains(tag)) {
                Collection<ProxiedPlayer> all = ProxyServer.getInstance().getPlayers();

                StringBuilder build = new StringBuilder();
                for (int i = 0; i < strings.length; i++) {
                    build.append(" ");
                    build.append(strings[i]);
                }
                String msg = build.toString();

                for (ProxiedPlayer target : all) {
                    if (Arrays.asList(staff).contains(TagAPI.getTag(target.getUniqueId().toString()))) {
                        target.sendMessage(ChatColor.RED + "[STAFF] " + getColor(tag) + player.getName() + ChatColor.GRAY + " : " + ChatColor.WHITE + msg);
                    }
                }
            }
        }
    }

    private static ChatColor getColor(String tag) {
        if (tag.equalsIgnoreCase("Master")) {
            return ChatColor.GOLD;
        } else if (tag.equalsIgnoreCase("Supervisor")) {
            return ChatColor.DARK_RED;
        } else if (tag.equalsIgnoreCase("Admin")) {
            return ChatColor.RED;
        } else if (tag.equalsIgnoreCase("Moderador")) {
            return ChatColor.DARK_GREEN;
        } else if (tag.equalsIgnoreCase("Suporte")) {
            return ChatColor.GREEN;
        } else {
            return ChatColor.GRAY;
        }
    }
}
