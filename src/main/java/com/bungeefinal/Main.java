package com.bungeefinal;

import com.bungeefinal.apis.MotdAPI;
import com.bungeefinal.apis.ServerAPI;
import com.bungeefinal.commands.CommandMotd;
import com.bungeefinal.commands.CommandStaff;
import com.bungeefinal.commands.CommandUpdateTag;
import com.bungeefinal.listeners.PlayerConnectEvents;
import com.bungeefinal.listeners.PlayerDisconnectEvents;
import com.bungeefinal.utils.ConfigurationUtils;
import com.bungeefinal.utils.SocketUtils;
import com.bungeefinal.vipAutentication.MySqlWeb;
import com.mrpowergamerbr.temmiewebhook.TemmieWebhook;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.event.PreLoginEvent;
import net.md_5.bungee.api.event.ProxyPingEvent;
import net.md_5.bungee.api.event.ServerKickEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;
import net.md_5.bungee.event.EventHandler;

public class Main extends Plugin implements Listener {

    public static TemmieWebhook privateTeam;
    public static String ip = "149.56.243.159";
    private static Main instance;
    public static Configuration config;
    private ConfigurationUtils cUtils = new ConfigurationUtils();

    public static Main getInstance() {
        return instance;
    }

    private static void setInstance(Main instance) {
        Main.instance = instance;
    }

    public void onEnable() {
        setInstance(this);
        try {
            config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(cUtils.load());
        } catch (Exception e) {
            e.printStackTrace();
        }
        privateTeam = new TemmieWebhook(config.getString("webhooks.privateTeam"));

        MySql.connect();
        MySqlWeb.connect();
        getProxy().getPluginManager().registerCommand(this, new CommandMotd());
        getProxy().getPluginManager().registerCommand(this, new CommandStaff());
        getProxy().getPluginManager().registerCommand(this, new CommandUpdateTag());
        getProxy().getPluginManager().registerListener(this, new PlayerConnectEvents());
        getProxy().getPluginManager().registerListener(this, new PlayerDisconnectEvents());
        getProxy().getPluginManager().registerListener(this, this);
        SocketUtils.online();
        MotdAPI.enableMotd();
    }

    @EventHandler
    public void onProxyPing(ProxyPingEvent event) {
        ServerPing ping = event.getResponse();
        if (!MotdAPI.get().getBackPing().isEmpty()) {
            ping.setVersion(new ServerPing.Protocol(ChatColor.translateAlternateColorCodes('&', MotdAPI.get().getBackPing()), event.getResponse().getVersion().getProtocol() - 1));
        }

        if (!MotdAPI.get().getPing().isEmpty()) {
            String[] pingInfos = MotdAPI.get().getPing().split("=");
            ServerPing.PlayerInfo[] sample = new ServerPing.PlayerInfo[pingInfos.length];
            for (int i = 0; i < sample.length; i++) {
                sample[i] = new ServerPing.PlayerInfo(ChatColor.translateAlternateColorCodes('&', pingInfos[i]), "");
            }
            ping.getPlayers().setSample(sample);
        }

        ping.setDescription(ChatColor.translateAlternateColorCodes('&', MotdAPI.get().getFirstLine() + "\n" + MotdAPI.get().getSecondLine()));
        event.setResponse(ping);
    }

    public void onDisable() {
        MySql.disconnect();
    }
}
