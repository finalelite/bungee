package com.bungeefinal.apis;

import com.bungeefinal.MySql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LocationAPI {

    public static void setInfo(String UUID, String server) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Player_location(UUID, Location) VALUES (?, ?)");
            st.setString(1, UUID);
            st.setString(2, server);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateServer(String UUID, String server) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("UPDATE Player_location SET Location = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setString(1, server);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static String getServer(String UUID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Player_location WHERE UUID = ?");
            st.setString(1, UUID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Location");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
