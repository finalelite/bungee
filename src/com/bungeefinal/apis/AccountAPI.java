package com.bungeefinal.apis;

import com.bungeefinal.MySql;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AccountAPI {

    public static void setAccountInfo(String UUID, String nick) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Account_data(UUID, Nick) VALUES (?, ?)");
            st.setString(1, UUID);
            st.setString(2, nick);
            //st.setBoolean(3, premium);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateNick(String UUID, String nick) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("UPDATE Account_data SET Nick = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setString(1, nick);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static String getNick(String UUID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Account_data WHERE UUID = ?");
            st.setString(1, UUID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Nick");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    // public static boolean getPremium(String UUID) {
    //        try {
    //            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Account_data WHERE UUID = ?");
    //            st.setString(1, UUID);
    //            ResultSet rs = st.executeQuery();
    //            while (rs.next()) {
    //                return rs.getBoolean("Premium");
    //            }
    //        } catch (SQLException e) {
    //            e.printStackTrace();
    //        }
    //        return false;
    //    }

    //public static String getUUID(String name) {
    //        String output = readURL("https://api.mojang.com/users/profiles/minecraft/" + name);
    //
    //        if (output.isEmpty()) {
    //            return null;
    //        } else {
    //            return output.substring(7, 39);
    //        }
    //    }
    //
    //    private static String readURL(String url) {
    //        try {
    //            HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
    //
    //            con.setRequestMethod("GET");
    //            con.setRequestProperty("User-Agent", "SkinsRestorer");
    //            con.setConnectTimeout(5000);
    //            con.setReadTimeout(5000);
    //            con.setDoOutput(true);
    //
    //            StringBuilder output = new StringBuilder();
    //            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
    //            String line;
    //            while ((line = in.readLine()) != null) {
    //                output.append(line);
    //            }
    //            in.close();
    //
    //            return output.toString();
    //        } catch (Exception e) {
    //            //-
    //        }
    //        return null;
    //    }
}
