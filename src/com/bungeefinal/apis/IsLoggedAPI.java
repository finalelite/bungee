package com.bungeefinal.apis;

import com.bungeefinal.MySql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class IsLoggedAPI {

    public static boolean getLoggedStatus(String UUID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Is_logged WHERE UUID = ?");
            st.setString(1, UUID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getBoolean("Logged");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void setLoggedStatus(String UUID, boolean status) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("INSERT INTO Is_logged (UUID, Logged) VALUES (?, ?)");
            st.setString(1, UUID);
            st.setBoolean(2, status);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static String getLoggedStatusExist(String UUID) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM Is_logged WHERE UUID = ?");
            st.setString(1, UUID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("UUID");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void updateLoggedStatus(String UUID, boolean status) {
        try {
            PreparedStatement st = MySql.con.prepareStatement("UPDATE Is_logged SET Logged = ? WHERE UUID = ?");
            st.setString(2, UUID);
            st.setBoolean(1, status);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
