package com.bungeefinal.apis;

import com.bungeefinal.MySql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MotdAPI {

    public static String getPing() {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM motd");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Ping");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getBackPing() {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM motd");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Back_ping");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getFirstLine() {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM motd");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("First_line");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getSecondLine() {
        try {
            PreparedStatement st = MySql.con.prepareStatement("SELECT * FROM motd");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Second_line");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
