package com.bungeefinal.listeners;

import com.bungeefinal.Main;
import com.bungeefinal.MySql;
import com.bungeefinal.apis.*;
import com.bungeefinal.utils.*;
import com.bungeefinal.vipAutentication.Authentication;
import com.mrpowergamerbr.temmiewebhook.DiscordMessage;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.*;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

public class PlayerConnectEvents implements Listener {

    //7
    @EventHandler
    public void onJoin(ServerConnectEvent event) {
        ProxiedPlayer player = event.getPlayer();
        String server = event.getTarget().getName().toString();
        String uuid = player.getUniqueId().toString();

        SocketUtils.online();

        if (LocationAPI.getServer(uuid) == null) {
            LocationAPI.setInfo(uuid, server);
        } else {
            BungeeCord.getInstance().getScheduler().schedule(Main.getInstance(), new Runnable() {
                @Override
                public void run() {
                    LocationAPI.updateServer(uuid, server);
                }
            }, 1, TimeUnit.SECONDS);
        }
    }

    //13
    @EventHandler
    public void onLogin(PostLoginEvent event) {
        ProxiedPlayer player = event.getPlayer();
        String uuid = player.getUniqueId().toString();

        if (AccountAPI.getNick(event.getPlayer().getUniqueId().toString()) == null) {
            AccountAPI.setAccountInfo(event.getPlayer().getUniqueId().toString(), event.getPlayer().getName());
        }

        String[] staff = {"Master", "Supervisor", "Admin", "Moderador", "Suporte"};
        if (Arrays.asList(staff).contains(TagAPI.getTag(player.getUniqueId().toString()))) {
            if (UserIP.getIP(player.getName()) == null || !UserIP.getIP(player.getName()).equalsIgnoreCase(player.getAddress().getHostString())) {
                String msg = CentralizeMsg.sendCenteredMessage(ChatColor.RED + "§lFinal Elite    \n \n" + ChatColor.RED + "Você não pode acessar esta conta!");
                player.disconnect(new TextComponent(msg));
                try {
                    PreparedStatement st = MySql.con.prepareStatement("SELECT discordId FROM Discord_info WHERE nick = ?");
                    st.setString(1, player.getName());
                    ResultSet rs = st.executeQuery();
                    if (rs.next()) {
                        DiscordMessage publicMessage = new DiscordMessage("Watchdog", ":warning: <@" + rs.getString("discordId") + "> alguém tentou fazer login em sua conta.", "https://i.imgur.com/vSrecgs.jpg");
                        Main.publicTeam.sendMessage(publicMessage);

                        DiscordMessage privateMessage = new DiscordMessage("Watchdog", ":warning: Acesso não permitido feito à conta de <@" + rs.getString("discordId") + "> (" + player.getName() + ") pelo IP " + player.getAddress().getHostString() + ".", "https://i.imgur.com/bpgg6oh.jpg");
                        Main.privateTeam.sendMessage(privateMessage);
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        if (UserIP.getIP(player.getName()) == null) {
            if (Authentication.permissionLoad(player.getName()) == false) {
                String msg = CentralizeMsg.sendCenteredMessage(ChatColor.RED + "§lFinal Elite\n \n§cServidor em fase\n§c BETA VIP." + ChatColor.RED + "\n \n \nAdquira VIP e obtenha acesso privilegiado\n" + ChatColor.AQUA + "www.finalelite.com.br");
                player.disconnect(new TextComponent(msg));
            }
        }

        if (PunirAPI.getExistPunicao(uuid) != null) {
            if (PunirAPI.getIsMutado(uuid) == false) {
                if (PunirAPI.getIsEterno(uuid) == true) {
                    String msg = CentralizeMsg.sendCenteredMessage(ChatColor.RED + "§lFinal Elite\n \n§cVocê está banido permanentemente do servidor.\nMotivo: " + ChatColor.DARK_GRAY + PunirAPI.getMotivo(uuid) + ChatColor.RED + "\nAutor: " + ChatColor.DARK_GRAY + PunirAPI.getAutor(uuid) + ChatColor.RED + "\n \nFoi punido incorretamente? Mande-nos uma DM no Twitter " + ChatColor.BLUE + "@FinalElite");
                    player.disconnect(new TextComponent(msg));
                } else {
                    if (PunirAPI.getTempo(uuid) >= 1) {
                        String msg = CentralizeMsg.sendCenteredMessage(ChatColor.RED + "§lFinal Elite\n \n§cVocê está banido temporariamente do servidor.\nMotivo: " + ChatColor.DARK_GRAY + PunirAPI.getMotivo(uuid) + ChatColor.RED + "\nDuração: " + ChatColor.DARK_GRAY + HorarioAPI.getTime(PunirAPI.getTempo(uuid)) + ChatColor.RED + "\nAutor: " + ChatColor.DARK_GRAY + PunirAPI.getAutor(uuid) + ChatColor.RED + "\n \nFoi punido incorretamente? Mande-nos uma DM no Twitter " + ChatColor.BLUE + "@FinalElite");
                        player.disconnect(new TextComponent(msg));
                    } else {
                        PunirAPI.removerPunicao(uuid);
                    }
                }
            }
        }
    }

    @EventHandler
    public void preLogin(PreLoginEvent event) {
        event.getConnection().setOnlineMode(false);
    }
}
